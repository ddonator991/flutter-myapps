import 'package:flutter/material.dart';

class ItemContainer extends StatelessWidget {
  final String imagePath;
  const ItemContainer({
    super.key,
    required this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(13),
        color: Colors.grey,
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'lib/images/tao.png',
              height: 158,
              width: 105,
            ),
          ],
        ),
      ),
    );
  }
}
