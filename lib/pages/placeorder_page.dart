// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:myapps/pages/confirm_page.dart';

class PlaceOrderPage extends StatelessWidget {
  const PlaceOrderPage({super.key});

  void _PlaceOrderPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const ConfirmPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 25),
              const Text(
                'Place Order',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                    icon: Image.asset('lib/images/blackdrink.png'),
                    iconSize: 80,
                    onPressed: () {
                      (context);
                    },
                  ),
                  const SizedBox(width: 25),
                  const Text(
                    'Monster Energy Classic\nPrice:150php',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              const Divider(
                height: 15,
                thickness: 3,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Builder(builder: (context) {
                    return const SizedBox(width: 25);
                  }),
                  const Text(
                    'Delivery Address',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Darcy Philip Garimbao | 09610798834',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Blk8 Lot20 villa Subdvision, Minien West Sta. Barbara',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Pangasinan, North Luzon',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 3,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  const Text(
                    'Standard Local',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(width: 150),
                  Text(
                    '60php',
                    style: TextStyle(
                      color: Colors.red[700],
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Receive by June 6-8',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 3,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Builder(builder: (context) {
                    return const Text(
                      'Order Total (2 item):',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    );
                  }),
                  const SizedBox(width: 71),
                  const Text(
                    '300php',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 3,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Builder(builder: (context) {
                    return const Text(
                      'Payment Option',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    );
                  }),
                  const SizedBox(width: 75),
                  const Text(
                    'Cash on Delivery',
                    style: TextStyle(
                      fontSize: 17,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 3,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Builder(builder: (context) {
                    return const Text(
                      'Payment Details',
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    );
                  }),
                ],
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  const Text(
                    'Merchandise Subtotal',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(width: 55),
                  Text(
                    '300php',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  const Text(
                    'Shipping Subtotal',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(width: 95),
                  Text(
                    '60php',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Builder(builder: (context) {
                    return const Text(
                      'Total Payment',
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    );
                  }),
                  const SizedBox(width: 90),
                  const Text(
                    '360php',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 50),
              ElevatedButton(
                onPressed: () {
                  _PlaceOrderPage(context);
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.black,
                  shape: const StadiumBorder(),
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 150),
                ),
                child: const Text(
                  "Confirm",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
