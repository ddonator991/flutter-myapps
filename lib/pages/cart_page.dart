// ignore_for_file: sort_child_properties_last

import 'package:flutter/material.dart';
import 'package:myapps/pages/placeorder_page.dart';

class CartPage extends StatelessWidget {
  const CartPage({super.key});

  // ignore: non_constant_identifier_names
  void _CartPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const PlaceOrderPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 25),
              const Text(
                'Cart',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 25),
              Image.asset(
                'lib/images/order.png',
                height: 150,
              ),
              const SizedBox(height: 500),
              ElevatedButton(
                onPressed: () {
                  _CartPage(context);
                },
                child: const Text(
                  "Confirm",
                  style: TextStyle(fontSize: 20),
                ),
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black,
                  shape: const StadiumBorder(),
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 150),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
