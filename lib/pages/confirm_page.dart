// ignore_for_file: non_constant_identifier_names, sort_child_properties_last

import 'package:flutter/material.dart';
import 'package:myapps/pages/home_page.dart';

class ConfirmPage extends StatelessWidget {
  const ConfirmPage({super.key});

  void _ConfirmPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const HomePage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 50),
              const Text(
                'Thank You',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 50,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 10),
              const Text(
                'For Purchase!',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 25),
              Image.asset(
                'lib/images/check.png',
                height: 400,
              ),
              const SizedBox(height: 150),
              ElevatedButton(
                onPressed: () {
                  _ConfirmPage(context);
                },
                child: const Text(
                  "Next",
                  style: TextStyle(fontSize: 20),
                ),
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black,
                  shape: const StadiumBorder(),
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 160),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
