import 'package:flutter/material.dart';

import 'package:myapps/pages/login_page.dart';

import 'package:myapps/components/my_textfield.dart';
import 'package:myapps/pages/forgotpass_page.dart';

class NewPassPage extends StatelessWidget {
  NewPassPage({super.key});

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  void NewPass1(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  void NewPass2(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ForgotPassPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 50),
              Text(
                'Changing Password',
                style: TextStyle(
                  color: Colors.grey[700],
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 100),
              MyTextField(
                controller: usernameController,
                hintText: 'Enter New Password',
                obscureText: true,
              ),
              const SizedBox(height: 10),
              MyTextField(
                controller: passwordController,
                hintText: 'Confirm Password',
                obscureText: true,
              ),
              const SizedBox(height: 100),
              ElevatedButton(
                onPressed: () {
                  NewPass1(context);
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.black,
                  shape: const StadiumBorder(),
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 120),
                ),
                child: const Text(
                  "Confirm",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              const SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Go back to',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: const TextStyle(fontSize: 15),
                    ),
                    onPressed: () {
                      NewPass2(context);
                    },
                    child: const Text('Login'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
