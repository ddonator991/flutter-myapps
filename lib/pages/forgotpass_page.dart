import 'package:flutter/material.dart';
import 'package:myapps/components/my_textfield.dart';
import 'package:myapps/pages/login_page.dart';
import 'package:myapps/pages/changepass_page.dart';

class ForgotPassPage extends StatelessWidget {
  ForgotPassPage({super.key});

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final contactnumberController = TextEditingController();
  final codeController = TextEditingController();

  void ForgotPassPage1(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NewPassPage()),
    );
  }

  void ForgotPassPage2(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 50),
              Text(
                'Verify Account',
                style: TextStyle(
                  color: Colors.grey[700],
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 100),
              MyTextField(
                controller: usernameController,
                hintText: 'Username',
                obscureText: false,
              ),
              const SizedBox(height: 10),
              MyTextField(
                controller: contactnumberController,
                hintText: 'Contact Number',
                obscureText: false,
              ),
              const SizedBox(height: 10),
              MyTextField(
                controller: codeController,
                hintText: 'Code',
                obscureText: false,
              ),
              const SizedBox(height: 100),
              ElevatedButton(
                onPressed: () {
                  ForgotPassPage1(context);
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.black,
                  shape: const StadiumBorder(),
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 120),
                ),
                child: const Text(
                  "Send to Email",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              const SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Go back to',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: const TextStyle(fontSize: 15),
                    ),
                    onPressed: () {
                      ForgotPassPage2(context);
                    },
                    child: const Text('Login'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
