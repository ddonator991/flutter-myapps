// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:myapps/pages/home_page.dart';
import 'package:myapps/pages/login_page.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  void ProfilePage1(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  void ProfilePage2(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomePage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 25),
              Text(
                'Profile',
                style: TextStyle(
                  color: Colors.grey[700],
                  fontSize: 50,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 25),
              Container(
                padding: EdgeInsets.all(16.0),
                child: CircleAvatar(
                  radius: 72.0,
                  backgroundColor: Colors.black,
                  backgroundImage: AssetImage('lib/images/tao.png'),
                ),
              ),
              SizedBox(height: 25),
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      SizedBox(width: 30),
                      Text(
                        'Name: Darcy Philip Garimbao',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 3,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  SizedBox(width: 30),
                  Text(
                    'Email: Darcy_Garimbao123@gmail.com',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 3,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  SizedBox(width: 30),
                  Text(
                    'Contact Number: 09610798834',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 3,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 150),
              ElevatedButton(
                onPressed: () {
                  ProfilePage1(context);
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black,
                  shape: StadiumBorder(),
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 150),
                ),
                child: const Text(
                  "Logout",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              SizedBox(height: 25),
              ElevatedButton(
                onPressed: () {
                  ProfilePage2(context);
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.green,
                  shape: StadiumBorder(),
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 150),
                ),
                child: Text(
                  "Go Back",
                  style: TextStyle(fontSize: 20),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
