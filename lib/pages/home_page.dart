// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:myapps/pages/cart_page.dart';
import 'package:myapps/components/item_container.dart';
import 'package:myapps/pages/profile_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  void _HomePage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const CartPage()),
    );
  }

  void _HomePage2(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const CartPage()),
    );
  }

  void _HomePage3(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const ProfilePage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      icon: Image.asset('lib/images/profile.png'),
                      iconSize: 40,
                      onPressed: () {
                        _HomePage3(context);
                      },
                    ),
                    const SizedBox(width: 240),
                    IconButton(
                      icon: Image.asset('lib/images/cart.png'),
                      iconSize: 40,
                      onPressed: () {
                        _HomePage2(context);
                      },
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Builder(builder: (context) {
                      return const SizedBox(width: 20);
                    }),
                    const Text(
                      'Items',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    ItemContainer(imagePath: 'lib/images/blackdrink.png'),
                    SizedBox(width: 50),
                    ItemContainer(imagePath: 'lib/images/yellowdrink.png'),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    SizedBox(width: 80),
                    Text(
                      'Classic',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(width: 110),
                    Text(
                      'Ultra Gold',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    SizedBox(width: 80),
                    Text(
                      '150php',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue,
                      ),
                    ),
                    SizedBox(width: 120),
                    Text(
                      '160php',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        _HomePage(context);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                      ),
                      child: const Text(
                        'Add to cart',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    const SizedBox(width: 80),
                    ElevatedButton(
                      onPressed: () {
                        _HomePage(context);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                      ),
                      child: const Text(
                        'Add to cart',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    ItemContainer(imagePath: 'lib/images/whitedrink.png'),
                    SizedBox(width: 50),
                    ItemContainer(imagePath: 'lib/images/purpledrink.png'),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    SizedBox(width: 65),
                    Text(
                      'Ultra White',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(width: 85),
                    Text(
                      'Ultra Violet',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    SizedBox(width: 80),
                    Text(
                      '170php',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue,
                      ),
                    ),
                    SizedBox(width: 115),
                    Text(
                      '170php',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        _HomePage(context);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                      ),
                      child: const Text(
                        'Add to cart',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    const SizedBox(width: 80),
                    ElevatedButton(
                      onPressed: () {
                        _HomePage(context);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                      ),
                      child: const Text(
                        'Add to cart',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
